from transformers import LlamaTokenizer


# 测试
print("---" + " test " + "---")
llama_tokenizer = LlamaTokenizer.from_pretrained("llama2", legacy=False)
print(len(llama_tokenizer))
lulu_llama_tokenizer = LlamaTokenizer.from_pretrained("merged_hf")
print(len(lulu_llama_tokenizer))

text = "智能技术模拟人类思维，实现自主学习和决策的计算机系统。"
print("--- llama ---")
# ['▁', '智', '能', '技', '<0xE6>', '<0x9C>', '<0xAF>', '模', '<0xE6>', '<0x8B>', '<0x9F>', '人', '类', '思', '<0xE7>', '<0xBB>', '<0xB4>', '，', '实', '现', '自', '主', '学', '<0xE4>', '<0xB9>', '<0xA0>', '和', '<0xE5>', '<0x86>', '<0xB3>', '<0xE7>', '<0xAD>', '<0x96>', '的', '计', '算', '机', '系', '统', '。']
print(llama_tokenizer.tokenize(text))      
print("--- lulu ---")
# ['▁', '智能', '技术', '模拟', '人类', '思维', '，', '实现', '自主', '学习和', '决策', '的', '计算机', '系统', '。']
print(lulu_llama_tokenizer.tokenize(text))
