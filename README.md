# lulu-tokenizer

从头开始训练 LLM 分词器，使用 SentencePiece 作为训练工具。

> [SentencePiece](https://github.com/google/sentencepiece) 是一种无监督文本 tokenizer 和 detokenizer，主要用于基于神经网络的文本生成系统，其中词汇量大小在神经模型训练之前预先确定。支持 BPE 和 UniLM 训练方法。

## 快速开始

```python
from transformers import LlamaTokenizer


# 测试
print("---" + " test " + "---")
llama_tokenizer = LlamaTokenizer.from_pretrained("llama2", legacy=False)
print(len(llama_tokenizer))
lulu_llama_tokenizer = LlamaTokenizer.from_pretrained("merged_hf")
print(len(lulu_llama_tokenizer))

text = "智能技术模拟人类思维，实现自主学习和决策的计算机系统。"
print("--- llama ---")
print(llama_tokenizer.tokenize(text))      
print("--- lulu ---")
print(lulu_llama_tokenizer.tokenize(text))
```

执行结果

```shell
--- test ---
32000
90947
--- llama ---
['▁', '智', '能', '技', '<0xE6>', '<0x9C>', '<0xAF>', '模', '<0xE6>', '<0x8B>', '<0x9F>', '人', '类', '思', '<0xE7>', '<0xBB>', '<0xB4>', '，', '实', '现', '自', '主', '学', '<0xE4>', '<0xB9>', '<0xA0>', '和', '<0xE5>', '<0x86>', '<0xB3>', '<0xE7>', '<0xAD>', '<0x96>', '的', '计', '算', '机', '系', '统', '。']
--- lulu ---
['▁', '智能', '技术', '模拟', '人类', '思维', '，', '实现', '自主', '学习和', '决策', '的', '计算机', '系统', '。']
```


## 执行流程

1. 下载数据

```shell
download_data.py
```

如果没条件执行下载数据，也可以从这里下载，然后解压放到 data 目录即可。
```shell
链接: https://pan.baidu.com/s/1R8vRZURxj0nTA5yLrSa6pA?pwd=mnck 提取码: mnck
```

2. 预处理数据

```shell
process_data.py
```

3. 训练分词器

```shell
run.sh  # 训练日志见 log.train.txt
```

4. 测试分词效果

```shell
segment_by_tokenizer.py
```

5. 对 LLaMa 进行词表扩充

```shell
merge_tokenizers.py
```

6. 测试 LLaMa 扩充效果

```shell
test_tokenizer.py
```

